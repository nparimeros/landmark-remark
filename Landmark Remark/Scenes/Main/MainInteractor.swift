//
//  MainInteractor.swift
//  Landmark Remark
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright (c) 2019 nicolas.parimeros. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import CoreLocation

protocol MainBusinessLogic {
    func loadUI(request: Main.LoadUI.Request)
    func loadNotes(request: Main.LoadNotes.Request)
    func saveNote(request: Main.SaveNote.Request)
}

protocol MainDataStore {
    var notes: [Note] { get }
}

class MainInteractor: MainBusinessLogic, MainDataStore {

    var presenter: MainPresentationLogic?
    var worker: MainWorker?
    var username: String?
    var notes: [Note] = []
    
    var locationManager: LocationManager?
    
    // MARK: Load UI

    func loadUI(request: Main.LoadUI.Request) {
        
        // Ask location manager to start updating location
        // This will ask for permission if needed
        locationManager?.startUpdatingLocation()

        guard let username = worker?.getUsername() else { return }

        self.username = username

        let response = Main.LoadUI.Response(username: username)
        presenter?.presentLoadUI(response: response)
    }
    
    // MARK: Load notes

    func loadNotes(request: Main.LoadNotes.Request) {

        worker?.loadNotes(completion: { [weak self] (success, notes) in

            // Handle error
            guard success else { return }

            // Keep notes
            self?.notes = notes

            // Ask presenter to present notes
            let response = Main.LoadNotes.Response(notes: notes)
            self?.presenter?.presentLoadNotes(response: response)
        })
    }

    // MARK: Save note

    func saveNote(request: Main.SaveNote.Request) {

        guard let text = request.note, !text.isEmpty, let username = username else { return }

        let note = Note(
            username: username,
            text: text,
            latitude: request.coordinate.latitude,
            longitude: request.coordinate.longitude
        )

        // Ask worker to save note
        worker?.save(note: note, completion: { [weak self] (success) in

            // Append note to current array
            self?.notes.append(note)

            // Ask presenter to present response
            let response = Main.SaveNote.Response(note: note)
            self?.presenter?.presentSaveNote(response: response)
        })
    }

}
