//
//  Note.swift
//  Landmark Remark
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

struct Note {

    var id: String
    var username: String
    var text: String
    var latitude: Double
    var longitude: Double

    init(id: String? = nil, username: String, text: String, latitude: Double, longitude: Double) {
        self.id = id ?? UUID().uuidString
        self.username = username
        self.text = text
        self.latitude = latitude
        self.longitude = longitude
    }
}
