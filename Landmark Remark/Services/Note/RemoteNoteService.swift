//
//  RemoteNoteService.swift
//  Landmark Remark
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation
import Firebase

protocol RemoteNoteService {
    func fetchNotes(completion: @escaping (_ success: Bool, _ notes: [Note]) -> Void)
    func save(note: Note, completion: @escaping (_ success: Bool) -> Void)
}

class FirebaseNoteService: RemoteNoteService {

    let ref = Database.database().reference()

    func fetchNotes(completion: @escaping (_ success: Bool, _ notes: [Note]) -> Void) {
        ref.child("notes").observeSingleEvent(of: .value) { (snapshot) in

            var notes: [Note] = []
            for child in snapshot.children.allObjects as! [DataSnapshot] {
                guard let noteDic = child.value as? [String: Any],
                    let username = noteDic["username"] as? String,
                    let text = noteDic["text"] as? String,
                    let latitude = noteDic["latitude"] as? Double,
                    let longitude = noteDic["longitude"] as? Double else { continue }
                let note = Note(id: child.key, username: username, text: text, latitude: latitude, longitude: longitude)
                notes.append(note)
            }

            return completion(true, notes)
        }
    }

    func save(note: Note, completion: @escaping (Bool) -> Void) {

        ref.child("notes").child(note.id).setValue([
            "username": note.username,
            "text": note.text,
            "latitude": note.latitude,
            "longitude": note.longitude,
        ])

        return completion(true)
    }

}
