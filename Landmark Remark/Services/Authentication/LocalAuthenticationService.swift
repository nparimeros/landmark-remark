//
//  AuthenticationService.swift
//  Landmark Remark
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

protocol LocalAuthenticationService {

    var currenrUsername: String? { get }
    
    var isAuthenticated: Bool { get }

    func save(username: String)
    
    func logout()

}

class LocalAuthenticationServiceUserDefaults: LocalAuthenticationService {

    let kUsernameKey = "username"
    let defaults = UserDefaults.standard
    
    var currenrUsername: String? {
        return defaults.string(forKey: kUsernameKey)
    }

    var isAuthenticated: Bool {
//        return false
        return currenrUsername != nil
    }

    func save(username: String) {
        defaults.set(username, forKey: kUsernameKey)
    }
    
    func logout() {
        defaults.removeObject(forKey: kUsernameKey)
    }

}
