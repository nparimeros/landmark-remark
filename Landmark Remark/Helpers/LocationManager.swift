//
//  LocationManager.swift
//  Landmark Remark
//
//  Created by Nicolas Parimeros on 23/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

//
//  LocationManager.swift
//  Landmark Remark
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import CoreLocation

protocol LocationManagerDelegate: class {
    func didChangeAuthorizationStatus(_ status: CLAuthorizationStatus)
}

class LocationManager: NSObject {
    
    static let shared = LocationManager()
    
    weak var delegate: LocationManagerDelegate?
    
    private let locationManager = CLLocationManager()
    
    var lastLocation: CLLocation?
    
    var isLocationAuthorizationDenied: Bool {
        let status = CLLocationManager.authorizationStatus()
        return status == .denied
    }
    
    var isLocationAuthorizationUndetermined: Bool {
        let status = CLLocationManager.authorizationStatus()
        return status == .notDetermined
    }
    
    private override init() {
        super.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 10
        locationManager.delegate = self
    }
    
    func startUpdatingLocation() {
        guard !isLocationAuthorizationDenied else { return }
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
        if isLocationAuthorizationUndetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
    
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        
        let date = location.timestamp
        let time = date.timeIntervalSinceNow
        
        guard fabs(time) < 15 else { return }
        
        lastLocation = location
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        delegate?.didChangeAuthorizationStatus(status)
    }
    
}
