//
//  LocalizeHelper.swift
//  Landmark Remark
//
//  Created by Nicolas Parimeros on 23/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

class LocalizeHelper {
    static func localize(_ key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
}
