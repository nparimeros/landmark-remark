//
//  Constants.swift
//  Landmark Remark
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

import Foundation

struct Constants {
    static let MY_INSTANCE_ADDRESS = "landmark-remark-nprs.us1a.cloud.realm.io"
    static let AUTH_URL = URL(string: "https://\(MY_INSTANCE_ADDRESS)/default")!
    static let REALM_URL = URL(string: "realms://\(MY_INSTANCE_ADDRESS)/Notes")!
}

struct LocalizedStringConstants {
    
    struct Global {
        static let buttonOk = LocalizeHelper.localize("BUTTON_OK")
        static let buttonCancel = LocalizeHelper.localize("BUTTON_CANCEL")
        static let buttonSettings = LocalizeHelper.localize("BUTTON_SETTINGS")
        static let placeholder = LocalizeHelper.localize("LOGIN_PLACEHOLDER_USERNAME")
    }

    struct Login {
        static let header = LocalizeHelper.localize("LOGIN_HEADER_USERNAME")
        static let placeholder = LocalizeHelper.localize("LOGIN_PLACEHOLDER_USERNAME")
    }
    
    struct Main {
        static let noLocationTitle = LocalizeHelper.localize("NO_LOCATION_ENABLED")
        static let noLocationMessage = LocalizeHelper.localize("ENABLE_LOCATION_SETTINGS")
        static let note = LocalizeHelper.localize("NOTE")
        static let newNote = LocalizeHelper.localize("NEW_NOTE")
        static let connectedAs = LocalizeHelper.localize("CONNECTED_AS")
        static let createNewNote = LocalizeHelper.localize("CREATE_NEW_NOTE")
    }
    
    struct Search {
        static let title = LocalizeHelper.localize("SEARCH_TITLE")
        static let searchbarPlaceholder = LocalizeHelper.localize("SEARCH_NOTE_TEXT_USERNAME")
    }

}
